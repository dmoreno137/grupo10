﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;

namespace Grupo10ACME
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            clnCalendar.DisplayDate = DateTime.Now;
            querys query = new querys();
            var retList = new List<string>();
            retList = query.queryCalendar();
            for (int i = 0; i < retList.Count; i++)
            {
                string[] fecha = retList[i].Split('-');
                var y = Convert.ToInt32(fecha[0].Replace(" ", ""));
                clnCalendar.BlackoutDates.Add(new CalendarDateRange(new DateTime(Convert.ToInt32(fecha[2].Replace(" ", "")), Convert.ToInt32(fecha[1].Replace(" ", "")), Convert.ToInt32(fecha[0].Replace(" ", "")))));
            }

            List<dgListado> listados = new List<dgListado>();
            listados = query.selectListado();

            dgListadoEmpresas.ItemsSource = listados;
        }

        private void btnDownload_Click(object sender, RoutedEventArgs e)
        {
            var fecha = clnCalendar.SelectedDate;
            UsingXmlReader((DateTime)fecha);
            querys query = new querys();
            List<dgListado> listados = new List<dgListado>();
            listados = query.selectListado();

            dgListadoEmpresas.ItemsSource = listados;

            var retList = new List<string>();
            retList = query.queryCalendar();
            clnCalendar.SelectedDate = null;
            for (int i = 0; i < retList.Count; i++)
            {
                string[] ofecha = retList[i].Split('-');
                var y = Convert.ToInt32(ofecha[0].Replace(" ", ""));
                clnCalendar.BlackoutDates.Add(new CalendarDateRange(new DateTime(Convert.ToInt32(ofecha[2].Replace(" ", "")), Convert.ToInt32(ofecha[1].Replace(" ", "")), Convert.ToInt32(ofecha[0].Replace(" ", "")))));
            }
            MessageBox.Show("Done!");
        }

        private static void UsingXmlReader(DateTime fecha)
        {
            var urlFecha = XmlReader.Create("https://www.boe.es/diario_borme/xml.php?id=BORME-S-" + fecha.Year + fecha.Month + fecha.Day);

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(urlFecha.BaseURI);
            string urlProvincia = string.Empty;
            foreach (XmlNode xmlNode in xmlDoc.DocumentElement.ChildNodes[1].ChildNodes[1].ChildNodes[0].ChildNodes)
            {

                foreach (XmlNode xmlNodeItem in xmlNode.LastChild.ChildNodes)
                {
                    urlProvincia = xmlNodeItem.InnerText;
                    urlProvincia = "https://www.boe.es/" + urlProvincia;
                    pdfReader pdfReader = new pdfReader();
                    string text = pdfReader.readPDF(urlProvincia);
                    var fechaArchivo = fecha.Day + "-" + fecha.Month + "-" + fecha.Year;
                    pdfReader.formatPDF(text, fechaArchivo);
                }

            }
        }

        private void txtFecha_KeyUp(object sender, KeyEventArgs e)
        {
            querys query = new querys();
            List<dgListado> listados = new List<dgListado>();
            var fecha = txtFecha.Text;
            listados = query.selectBuscar(fecha);
            dgListadoEmpresas.ItemsSource = "";
            dgListadoEmpresas.ItemsSource = listados;
        }
    }
}