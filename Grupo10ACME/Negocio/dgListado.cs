﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grupo10ACME
{
    class dgListado
    {
        public string empresa { get /*=> empresa*/; set /*=> empresa = value*/; }
        public string provincia { get/* => provincia*/; set/* => provincia = value*/; }
        public string capital { get/* => capital*/; set/* => capital = value*/; }
        public string fecha { get/* => fecha*/; set/* => fecha = value*/; }

        public dgListado(string lEmpresa, string lProvincia, string lCapital, string lFecha)
        {
            empresa = lEmpresa;
            provincia = lProvincia;
            capital = lCapital;
            fecha = lFecha;
        }
    }
}
