﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Grupo10ACME.Model;

namespace Grupo10ACME
{
    class querys
    {
        public void descargar(string empresa, string provincia, string capital, string fecha)
        {
            using (Grupo10 db = new Grupo10())
            {
                nuevasEmpresas oNuevasEmpresas = new nuevasEmpresas();
                oNuevasEmpresas.empresa = empresa;
                oNuevasEmpresas.provincia = provincia;
                oNuevasEmpresas.capital = capital;
                oNuevasEmpresas.fecha = fecha;
                db.nuevasEmpresas.Add(oNuevasEmpresas);
                db.SaveChanges();
            }
        }

        public List<string> queryCalendar()
        {
            using (Grupo10 db = new Grupo10())
            {
                var lst = db.nuevasEmpresas.GroupBy(s => s.fecha).ToList();
                var retList = new List<string>();
                foreach(var ofechas in lst)
                {
                    retList.Add(ofechas.Key);
                    int i = 0;
                    i++;
                }

                return retList;
            }
            
        }

        public List<dgListado> selectListado()
        {
            List<dgListado> listados = new List<dgListado>();
            using (Grupo10 db = new Grupo10())
            {
                var lst = db.nuevasEmpresas;
                foreach (var oNuevasEmpresas in lst)
                {
                    listados.Add(new dgListado(oNuevasEmpresas.empresa, oNuevasEmpresas.provincia, oNuevasEmpresas.capital, oNuevasEmpresas.fecha));
                }
            }
            return listados;
        }

        public List<dgListado> selectBuscar(string bFecha)
        {
            List<dgListado> listados = new List<dgListado>();
            using (Grupo10 db = new Grupo10())
            {
                var lst = db.nuevasEmpresas.Where(h => h.fecha.Contains(bFecha));
                foreach(var oNuevasEmpresas in lst)
                {
                    listados.Add(new dgListado(oNuevasEmpresas.empresa, oNuevasEmpresas.provincia, oNuevasEmpresas.capital, oNuevasEmpresas.fecha));
                }
            }
            return listados;
        }
    }
}
